import { email, required, sameAs, maxLength, minLength } from '@vuelidate/validators';
import { NAME_REGEX, PASSWORD_REGEX } from '../models/custom-form-rules';

export const authFormRules = {
    email: {
        required,
        email
    },
    password: {
        required,
        // PASSWORD_REGEX,
        minLength: minLength(8)
    }
}

export const registerFormRules = {
    surname: {
        required,
        NAME_REGEX,
        maxLength: maxLength(50)
    },
    name: {
        required,
        NAME_REGEX,
        maxLength: maxLength(50)
    },
    email: {
        email,
        required
    },
    password: {
        required,
        PASSWORD_REGEX,
        minLength: minLength(8)
    },
    repeatedPassword: {
        required,
        PASSWORD_REGEX,
        minLength: minLength(8)
    },
    confirmation: {
        required,
        sameAs: sameAs(true)
    }
}