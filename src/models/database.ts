import type { Database } from '../abstracts/database';

export const database: Database[] = [
    {
        image: '/images/db/sfh.png',
        name: 'СФХ',
        path: '/sfhs'
    },
    {
        image: '/images/db/vulnerability.png',
        name: 'Уязвимости',
        path: '/vulnerabilities'
    },
    {
        image: '/images/db/attack.png',
        name: 'Атаки',
        path: '/attacks'
    },
    // {
    //     image: '/images/db/object.png',
    //     name: 'Объекты',
    //     path: '/objects'
    // },
    {
        image: '/images/db/risk.png',
        name: 'Риски',
        path: '/risks'
    },
    {
        image: '/images/db/threat.png',
        name: 'Угрозы',
        path: '/threats'
    },
];