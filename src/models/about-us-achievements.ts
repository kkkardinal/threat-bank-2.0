import type { InfoBlock } from '../abstracts/info-block';

export const aboutUsAchievements: InfoBlock[] = [
    {
        image: '/images/backgrounds/bg-1.jpg',
        name: 'Гранты РНФ',
        description: 'гранты направлены на проведение фундаментальных научных \n' +
            'исследований и поисковых научных исследований отдельными научными группами',
        path: ''
    },
    {
        image: '/images/backgrounds/bg-2.jpg',
        name: 'Апробированные проекты',
        description: 'результаты научных исследований протестированы в \n' +
            'реальных условиях, доработаны и внедрены в предприятия',
        path: ''
    },
    {
        image: '/images/backgrounds/bg-3.jpg',
        name: 'Диссертации',
        description: 'узконаправленные работы на соискание ученой степени кандидата \n' +
            'технических наук, которые получились в результате работы над научными проектами',
        path: ''
    },
    {
        image: '/images/backgrounds/bg-3.jpg',
        name: 'Выпускные квалификационные работы',
        description: 'дипломы являются результатом поисковых научных исследований',
        path: ''
    },
    {
        image: '/images/backgrounds/bg-1.jpg',
        name: 'Курсовые работы',
        description: 'все исследования ведутся в рамках проектов, которые реализуются \n' +
            'научной группой',
        path: ''
    },
    {
        image: '/images/backgrounds/bg-2.jpg',
        name: 'Выступления на конференциях и публикация статей',
        description: 'индексируются в РИНЦ, ВАК, Scopus и Web of Science',
        path: ''
    }
]