import type { AuthFormData, RegisterFormData } from '../abstracts/form-data';

export const authFormData: AuthFormData = {
    email: '',
    password: ''
};

export const registerFormData: RegisterFormData = {
    // surname: '',
    // name: '',
    email: '',
    password: '',
    repeatedPassword: '',
    confirmation: false
}

export const threatModelForm = {
    name: '',
    domain: '',
    characteristics: [],
    sfcs: ''
}