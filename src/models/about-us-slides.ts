import type { LinkBlock } from '../abstracts/link-block';

export const aboutUsSlides: LinkBlock[] = [
    {
        image: '/images/about-us/slider-1.jpg',
        name: 'Интервью Басан на тему кибербезопасность',
        link: 'https://sfedu.ru/press-center/news/66711'
    },
    {
        image: '/images/about-us/slider-2.jpg',
        name: 'Ученые из Ростова-на-Дону создают адаптивную систему защиты от кибератак',
        link: 'https://www.minobrnauki.gov.ru/press-center/news/novosti-podvedomstvennykh-uchrezhdeniy/35184/'
    },
    {
        image: '/images/about-us/slider-3.jpg',
        name: 'Наука в лицах ЮФУ: Елена Сергеевна Басан и информационная безопасность в беспроводных сетях',
        link: 'https://sfedu.ru/press-center/news/69918'
    },
    {
        image: '/images/about-us/link-laptop.jpg',
        name: 'Газета «Город N». Учим беспилотники принципам рефлексии',
        link: 'https://www.gorodn.ru/razdel/obshchestvo_free/obrazovanie/38945/'
    },
    {
        image: '/images/about-us/slider-5.jpg',
        name: 'В России придумали, как защитить беспилотники от удаленного взлома',
        link: 'https://ria.ru/20211111/yufu-1758408933.html'
    },
    {
        image: '/images/about-us/slider-6.png',
        name: 'Елена Басан о месте человека в "кибер" мире',
        link: 'https://vk.com/sfedu_official?w=wall-47535294_27791'
    },
    {
        image: '/images/about-us/slider-8.jpg',
        name: '«Задачей стартапа является обнаружение утечки бытового газа»',
        link: 'https://gorodn.ru/razdel/obshchestvo_free/obrazovanie/39749/'
    },
    {
        image: '/images/about-us/slider-9.jpg',
        name: '16 стартап-проектов ЮФУ стали победителями конкурса «Студенческий стартап»',
        link: 'https://news.myseldon.com/ru/news/index/274110675'
    },
    {
        image: '/images/about-us/slider-10.png',
        name: 'Исследования Басан Е.С. на ресурсе Prosto',
        link: 'https://www.youtube.com/watch?v=l4xTRUoKtOU'
    }
];