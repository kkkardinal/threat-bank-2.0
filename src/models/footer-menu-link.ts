import type {MenuLink} from "../abstracts/menu-link";

export const footerMenuLink: MenuLink[] = [
    {
        name: 'О нас',
        link: '/about-us'
    },
    {
        name: 'Политика конфиденциальности',
        link: '/privacy-policy'
    },
    {
        name: 'Сервисы',
        link: '/services'
    },
    {
        name: 'Базы данных угроз',
        link: '/bd'
    }
]