import type { LinkBlock } from '../abstracts/link-block';

export const aboutUsLinks: LinkBlock[] = [
    {
        image: '/images/about-us/link-laptop.jpg',
        name: 'Как помочь себе обезопасить свой компьютер от взлома?',
        link: ''
    },
    {
        image: '/images/about-us/link-basan.jpg',
        name: 'Интервью Елены Басан на тему “Кибербезопасность”',
        link: 'https://sfedu.ru/press-center/news/66711'
    },
    {
        image: '/images/about-us/link-attacks.jpg',
        name: 'Популярные атаки за 2022 год',
        link: ''
    },
    {
        image: '/images/about-us/link-red-basan.jpg',
        name: 'Интервью Елены Басан на тему “Основы безопасности”',
        link: ''
    },
    {
        image: '/images/about-us/link-data.jpg',
        name: 'Утечки данных и как с этим бороться',
        link: ''
    },
    {
        image: '/images/about-us/link-team.jpg',
        name: 'Интервью Евгения Абрамова на тему “Цифровые  технологии”',
        link: ''
    },
    {
        image: '/images/about-us/link-mouse.jpg',
        name: 'Да здравствуют отечественные продукты в мире IT',
        link: ''
    },
    {
        image: '/images/about-us/link-ishukova.jpg',
        name: 'Интервью Евгении Ищуковой на тему “Блокчейн технологии”',
        link: ''
    }
];