export const systemRequirementsName = [
    'Операционная система:',
    'Процессор:',
    'Оперативная память:',
    'Память на Жестком Диске:',
    'Python:',
    'Утилита:',
    'Библиотеки:',
];

export const systemRequirementsDescription = [
    'Linux Ubuntu 22.04',
    '2-х ядерный с частотой 2 ГГц и выше',
    '4 Гб',
    '25 Гб',
    '3.10',
    'nmap версии 7.91+dfsg1+really7.80+dfsg1-2build',
    'python xmltodict версии 0.13.0,\n' +
    'python scapy версии 2.4.5.',
];