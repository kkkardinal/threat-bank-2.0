import type { ILimitItem } from '../abstracts/limit-item';

export const limitList: ILimitItem[] = [
    {
        count: 10,
        isActive: true
    },
    {
        count: 20,
        isActive: false
    },
    {
        count: 50,
        isActive: false
    }
]