import { helpers } from '@vuelidate/validators';

export const PHONE_REGEX = helpers.regex(/^\+?(\d{1,3})?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/);

export const NAME_REGEX = helpers.regex(/^[a-zA-Zа-яёА-ЯЁ ]+$/u);

export const PASSWORD_REGEX = helpers.regex(/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}/g);