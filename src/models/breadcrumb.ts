import type { IBreadcrumb } from '../abstracts/breadcrumb';

export const servicesBreadcrumb: IBreadcrumb[] = [
    {
        name: 'Сервисы',
        path: '/services'
    },
]

export const databaseBreadcrumb: IBreadcrumb[] = [
    {
        name: 'База данных угроз',
        path: '/bd'
    },
]

export const attackBreadcrumb:IBreadcrumb[] = [
    ...databaseBreadcrumb,
    {
        name: 'Атаки',
        path: '/attacks'
    }
];

export const incidentsBreadcrumb:IBreadcrumb[] = [
    ...databaseBreadcrumb,
    {
        name: 'Инциденты',
        path: '/incidents'
    }
];

export const objectsBreadcrumb:IBreadcrumb[] = [
    ...databaseBreadcrumb,
    {
        name: 'Объекты',
        path: '/objects'
    }
];

export const risksBreadcrumb:IBreadcrumb[] = [
    ...databaseBreadcrumb,
    {
        name: 'Риски',
        path: '/risks'
    }
];

export const sfhsBreadcrumb:IBreadcrumb[] = [
    ...databaseBreadcrumb,
    {
        name: 'СФХ',
        path: '/sfhs'
    }
];

export const threatsBreadcrumb:IBreadcrumb[] = [
    ...databaseBreadcrumb,
    {
        name: 'Угрозы',
        path: '/threats'
    }
];

export const vulnerabilitiesBreadcrumb:IBreadcrumb[] = [
    ...databaseBreadcrumb,
    {
        name: 'Уязвимости',
        path: '/vulnerabilities'
    }
];

export const sfhServiceBreadcrumb: IBreadcrumb[] = [
    ...servicesBreadcrumb,
    {
        name: 'Сервис анализа СФХ',
        path: '/sfh-service'
    }
]

export const vulnerabilityServiceBreadcrumb: IBreadcrumb[] = [
    ...servicesBreadcrumb,
    {
        name: 'Сервис формирования перечня уязвимостей',
        path: '/vulnerability-service'
    }
];

export const vectorAttackServiceBreadcrumb: IBreadcrumb[] = [
    ...servicesBreadcrumb,
    {
        name: 'Сервис формирования вектора атак',
        path: '/vector-attack-service'
    }
];

export const threatModelServiceBreadcrumb: IBreadcrumb[] = [
    ...servicesBreadcrumb,
    {
        name: 'Сервис формирования модели угроз',
        path: '/vector-attack-service'
    }
];