import type { InfoBlock } from '../abstracts/info-block';

export const services: InfoBlock[] = [
    {
        image: '/images/backgrounds/bg-2.jpg',
        name: 'Сервис анализа СФХ',
        description: 'Данный сервис позволит определить версию и тип структурно-функциональных характеристик вашего устройства \n' +
            'и отслеживать их изменения',
        path: '/sfh-service'
    },
    {
        image: '/images/backgrounds/bg-1.jpg',
        name: 'Сервис формирования перечня уязвимостей',
        description: 'Сервис поможет определить возможные уязвимости для вашего устройства или системы на данный момент',
        path: '/vulnerability-service'

    },
    {
        image: '/images/backgrounds/bg-3.jpg',
        name: 'Сервис формирования вектора атаки',
        description: 'Сервис позволит определить, каким атакам может быть подвержена ваша система. Также предложит способы улучшить безопасность устройства от возможных атак',
        path: '/vector-attack-service'
    },
    {
        image: '/images/backgrounds/bg-4.jpg',
        name: 'Сервис формирования модели угроз',
        description: 'На основании данных о структурно-функциональных характеристиках вашего устройства сервис сформирует актуальную модель угроз вашей системы',
        path: '/threat-model-service'
    },
];