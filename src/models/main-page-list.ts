import type {PageItem} from "@/abstracts/page-item";

export const MainPageList: PageItem[] = [
    {
        route: '/about-us',
        img: '/images/main-about-us.jpg',
        title: 'О нас',
        description: 'Наша команда состоит из опытных разработчиков,\n' +
            '              аналитиков и экспертов в области информационной и кибербезопасности.',
        isInvert: true,
    },
    {
        route: '/statistic',
        externalLink: 'https://cybermap.kaspersky.com/ru',
        img: '/images/main-list-1.png',
        title: 'Статистика  ',
        description: 'Каждую секунду в мире фиксируются киберугрозы. Лаборатория Касперского собирает \n' +
            'данные пользователей,  а также \n' +
            'со своих ханипотов. Подробнее об актуальных киберугрозах на странице статистики.',
        isInvert: false,
    },
    {
        route: '/services',
        img: '/images/main-list-2.png',
        title: 'Сервисы  ',
        description: 'Сканирование системы и выявление структурно-функциональных характеристик, поиск \n' +
            'актуальных уязвимостей, построение векторов атак, формирование модели угроз с описанием точек входа.',
        isInvert: true
    },
    {
        route: '/bd',
        img: '/images/main-list-3.png',
        title: 'База данных  ',
        description: 'База содержит в себе структурированные данные о структурно-функциональных \n' +
            'характеристиках киберфизических систем, а также актуальные для них уязвимости, атаки, \n' +
            'угрозы и риски безопасности.',
        isInvert: false
    },
];
