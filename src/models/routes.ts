import type { RouteRecordRaw } from 'vue-router';
import Main from '../pages/Main.vue';
// import AboutUs from '../pages/AboutUs.vue';
// import Statistic from '../pages/Statistic.vue';
// import AuthPage from '../pages/AuthPage.vue';
// import Services from '../pages/Services.vue';
// import ErrorPage from '../pages/ErrorPage.vue';
// import DataBase from '../pages/db/DataBase.vue';
// import Attacks from "../pages/db/Attacks.vue";
// import Objects from "../pages/db/Objects.vue";
// import Incidents from "../pages/db/Incidents.vue";
// import Risks from "../pages/db/Risks.vue";
// import Sfhs from "../pages/db/Sfhs.vue";
// import Threats from "../pages/db/Threats.vue";
// import Vulnerabilities from "../pages/db/Vulnerabilities.vue";
// import SfhService from "../pages/sfh-service/SfhService.vue";
// import VulnerabilityService from "../pages/vulnerability-service/VulnerabilityService.vue";
// import Account from "../pages/Account.vue";
// import VectorAttackService from "../pages/vector-attack-service/VectorAttackService.vue";
// import ThreatModelService from "../pages/threat-model-service/ThreatModelService.vue";

const routes: RouteRecordRaw[] = [
    {
        name: 'Main page',
        path: '/',
        component: Main,
        meta: {
            requiredAuth: false
        }
    },
    // {
    //     name: 'Statistic page',
    //     path: '/statistic',
    //     component: Statistic,
    //     meta: {
    //         requiredAuth: false
    //     }
    // },
    // {
    //     name: 'About us page',
    //     path: '/about-us',
    //     component: AboutUs,
    //     meta: {
    //         requiredAuth: false
    //     }
    // },
    // {
    //     name: 'Services page',
    //     path: '/services',
    //     component: Services,
    //     meta: {
    //         requiredAuth: false
    //     }
    // },
    // {
    //     name: 'Account',
    //     path: '/account',
    //     component: Account,
    //     meta: {
    //         requiredAuth: false
    //     }
    // },
    // {
    //     name: 'sfh Services page',
    //     path: '/sfh-service',
    //     component: SfhService,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Vulnerability Service page',
    //     path: '/vulnerability-service',
    //     component: VulnerabilityService,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Auth page',
    //     path: '/auth',
    //     component: AuthPage,
    //     meta: {
    //         requiredAuth: false
    //     }
    // },
    // {
    //     name: 'Data base page',
    //     path: '/bd',
    //     component: DataBase,
    //     meta: {
    //         requiredAuth: false
    //     }
    // },
    // {
    //     name: 'Attacks page',
    //     path: '/attacks',
    //     component: Attacks,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Objects page',
    //     path: '/objects',
    //     component: Objects,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Incidents page',
    //     path: '/incidents',
    //     component: Incidents,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Risks page',
    //     path: '/risks',
    //     component: Risks,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Sfhs page',
    //     path: '/sfhs',
    //     component: Sfhs,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Threats page',
    //     path: '/threats',
    //     component: Threats,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Vulnerabilities page',
    //     path: '/vulnerabilities',
    //     component: Vulnerabilities,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Vector Attack page',
    //     path: '/vector-attack-service',
    //     component: VectorAttackService,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Threat model service',
    //     path: '/threat-model-service',
    //     component: ThreatModelService,
    //     meta: {
    //         requiredAuth: true
    //     }
    // },
    // {
    //     name: 'Error page',
    //     path: '/:catchAll(.*)',
    //     component: ErrorPage,
    //     meta: {
    //         requiredAuth: false
    //     }
    // }
];

export default routes;
