// import axios from 'redaxios';
// import {getHost} from "@/utils/get-host";
//
// export class ApiRequest {
//     private url: string = getHost();
//     private api = axios.create({
//         baseURL: this.url,
//         withCredentials: true
//     });
//
//     public async get(path: string) {
//         try {
//             const response = await this.api.get(this.url + path);
//             return response;
//         } catch (err: any) {
//             if (err.response && err.response.status === 401) {
//                 // Попытка обновить токен
//                 try {
//                     await this.post('auth/token/refresh/', {});
//                     // Повторный запрос после обновления токена
//                     const retryResponse = await this.api.get(this.url + path);
//                     return retryResponse;
//                 } catch (refreshErr: any) {
//                     if (refreshErr.response?.data?.detail === 'No valid refresh token found.') {
//                         window.location.href = '/auth';
//                     }
//                 }
//             }
//             throw err; // Возвращаем ошибку, если она не связана с токеном
//         }
//     }
//
//     public async post<T>(path: string, data: T) {
//         try {
//             const response = await this.api.post(this.url + path, data);
//             return response; // Возвращаем респонс сразу, если запрос успешен
//         } catch (err: any) {
//             if (err.response && err.response.data.detail === 'No valid refresh token found.') {
//                 window.location.href = '/auth';
//             } else if (err.response && err.response.status === 401) {
//                 try {
//                     // Попытка обновить токен
//                     await this.post('auth/token/refresh/', {});
//                     // Повторный запрос после обновления токена
//                     const retryResponse = await this.api.post(this.url + path, data);
//                     return retryResponse;
//                 } catch (refreshErr: any) {
//                     if (refreshErr.response?.data?.detail === 'No valid refresh token found.') {
//                         window.location.href = '/auth';
//                     }
//                     throw refreshErr; // Возвращаем ошибку, если обновление токена не удалось
//                 }
//             }
//             throw err; // Возвращаем ошибку, если она не связана с токеном
//         }
//     }
// }

import axios from 'redaxios';
import { getHost } from "@/utils/get-host";

export class ApiRequest {
    private url: string = getHost();
    private api = axios.create({
        baseURL: this.url,
        withCredentials: true
    });

    // Общий обработчик ошибок
    private handleError(error: any): void {
        // Редирект на /auth при любой ошибке
        // if (error.data.email[0] != 'Пользователь с таким email уже существует.') {
        //     window.location.href = '/auth';
        // }
        console.error("API Error:", error); // Логируем ошибку
        window.location.href = '/auth';

    }

    public async get(path: string) {
        try {
            const response = await this.api.get(this.url + path);
            return response;
        } catch (err: any) {
            if (err.response && err.response.status === 401) {
                try {
                    // Попытка обновить токен
                    await this.post('auth/token/refresh/', {});
                    // Повторный запрос после обновления токена
                    const retryResponse = await this.api.get(this.url + path);
                    return retryResponse;
                } catch {
                    this.handleError(err.response);
                }
            } else {
                this.handleError(err.response);
            }
        }
    }

    public async post<T>(path: string, data: T) {
        try {
            const response = await this.api.post(this.url + path, data);
            return response;
        } catch (err: any) {
            // console.log(err)
            if (err.response && err.response.status === 401) {
                try {
                    // Попытка обновить токен
                    await this.post('auth/token/refresh/', {});
                    // Повторный запрос после обновления токена
                    const retryResponse = await this.api.post(this.url + path, data);
                    return retryResponse;
                } catch {
                    this.handleError(err.response);
                }
            } else {
                this.handleError(err.response);
            }
        }
        // return response;
    }
}

