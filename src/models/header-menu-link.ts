import type {MenuLink} from "../abstracts/menu-link";

export const headerMenuLink: MenuLink[] = [
    {
        name: 'Главная',
        link: '/'
    },
    {
        name: 'Сервисы',
        link: '/services'
    },
    {
        name: 'База данных',
        link: '/bd'
    }
]