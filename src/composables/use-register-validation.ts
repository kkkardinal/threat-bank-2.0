import { computed } from 'vue';
import type { RegisterFormData } from '../abstracts/form-data';
import type { ValidationArgs } from '@vuelidate/core';
import { registerFormRules } from '../models/form-rules';
import useVuelidate from '@vuelidate/core';

export default function useRegisterFormValidation(formData: RegisterFormData) {
    const registerValidator = useVuelidate<RegisterFormData, ValidationArgs>(registerFormRules, formData);
    const hasFormError = computed(() => registerValidator.value.form.$invalid || registerValidator.value.form.$error);

    const emailErrorMessage = computed(() => {
        if (!registerValidator.value.email.$model) {
            return 'Укажите электронную почту'
        } else {
            return 'Проверьте корректность почты'
        }
    })

    const firstPasswordErrorMessage = computed(() => {
        if (!registerValidator.value.password.$model) {
            return 'Укажите пароль'
        } else {
            return 'Пароль должен быть длинною не менее 8 и содержать латинские буквы верхнего и нмжнего регистра, цифры и символы'
        }
    })

    const repeatedPasswordErrorMessage = computed(() => {
        if (!registerValidator.value.repeatedPassword.$model) {
            return 'Повторите пароль';
        }

        if (registerValidator.value.repeatedPassword.$model != registerValidator.value.password.$model) {
            return 'Пароли не совпадают'
        }
    });

    const nameErrorMessage = computed(() => {
        if (!registerValidator.value.name.$model) {
            return 'Укажите имя';
        }
    });

    const surnameErrorMessage = computed(() => {
        if (!registerValidator.value.surname.$model) {
            return 'Укажите фамилию';
        }
    });

    const hasAuthEmptyField = computed(() => !registerValidator.value.login.$model || !registerValidator.value.password.$model);

    return {
        firstPasswordErrorMessage,
        emailErrorMessage,
        hasAuthEmptyField,
        registerValidator,
        hasFormError,
        repeatedPasswordErrorMessage,
        nameErrorMessage,
        surnameErrorMessage
    };
}
