import { computed } from 'vue';
import type { AuthFormData } from '../abstracts/form-data';
import type { ValidationArgs } from '@vuelidate/core';
import useVuelidate from '@vuelidate/core';
import { authFormRules } from '../models/form-rules';

export default function useAuthFormValidation(formData: AuthFormData) {
    const authValidator = useVuelidate<AuthFormData, ValidationArgs>(authFormRules, formData);
    const hasFormError = computed(() => authValidator.value.form.$invalid || authValidator.value.form.$error);

    const loginErrorMessage = computed(() => {
        if (!authValidator.value.email.$model) {
            return 'Укажите электронную почту';
        } else {
            return 'Проверьте корректность логина';
        }
    })

    const passwordErrorMessage = computed(() => {
        if (!authValidator.value.password.$model) {
            return 'Укажите пароль'
        }
        // else {
        //     return 'Пароль должен быть длинною не менее 8 и содержать латинские буквы верхнего и нмжнего регистра, цифры и символы'
        // }
    })

    const hasAuthEmptyField = computed(() => !authValidator.value.email.$model || !authValidator.value.password.$model);

    return {
        passwordErrorMessage,
        loginErrorMessage,
        hasAuthEmptyField,
        authValidator,
        hasFormError
    };
}
