import './assets/main.css'
import VueTablerIcons from "vue-tabler-icons";
import ElementPlus from 'element-plus';
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'element-plus/dist/index.css';
import './assets/reset.css';
import './assets/main.css';
import Paginate from "vuejs-paginate-next";
import 'element-plus/theme-chalk/index.css'; // Подключаем базовые стили Element Plus

const app = createApp(App)

app.use(router)
app.use(VueTablerIcons)
app.use(ElementPlus)
app.component('Paginate', Paginate)
app.mount('#app')
