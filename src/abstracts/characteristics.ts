export interface CharacteristicsState {
    characteristics: Characteristic[],
    count: number
}

export interface Characteristic {
    id: number,
    name: string,
    criticality_level: number
}