export interface AboutUsDescription {
    image: string,
    text: string,
    isInvert: boolean
}