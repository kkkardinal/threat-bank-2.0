export interface AuthFormData {
    email: string,
    password: string
}

export interface RegisterFormData {
    // surname: string,
    // name: string,
    email: string,
    password: string,
    repeatedPassword: string,
    confirmation: boolean
}