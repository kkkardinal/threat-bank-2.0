export interface Database {
    image: string;
    name: string;
    path: string;
}