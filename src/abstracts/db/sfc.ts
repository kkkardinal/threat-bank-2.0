import type {SfcType} from "./sfc-type";
import type {Source} from "./source";
import type {Attack} from "./attacks.ts";
import type {Vulnerability} from "./vulnerability.ts";

export interface Sfc {
    id: number,
    sfc_type: SfcType,
    sources: Source[],
    name: string,
    description: string,
    name_in_system: string,
    version: string,
    criticality_level: number,
    destabilization_level: number,
    archetypes: string,
    certificate_validity_date: string,
    lft: number,
    rght: number,
    tree_id: number,
    level: number,
    parent: number
}

export interface SfcState {
    count: number,
    limit: number,
    next: string | null,
    previous: string | null,
    sfcs: Sfc[],
    ordering: string,
    search: string,
    offset: number
}

export interface UserSfcState {
    sfcs: Sfc[],
    count: number,
}

export interface UserDataState {
    sfcs: Sfc[],
    attacks: Attack[],
    vulnerabilities: Vulnerability[],
    attackVectors: any[]
}
