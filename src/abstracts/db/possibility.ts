import type {Intruder} from "./intruder";

export interface Possibility {
    id: number,
    intruder: Intruder
    name: string,
    potential_level: number
}