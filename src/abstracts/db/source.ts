export interface Source {
    id: number,
    name: string,
    url: string,
    type: number,
    sfc: number
}