import type {Intruder} from "./intruder";

export interface Motive {
    id: number,
    intruder: Intruder,
    name: string
}