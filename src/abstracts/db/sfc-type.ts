import type { SfcClass } from './sfc-class';

export interface SfcType {
    id: number,
    sfc_class: SfcClass,
    name: string,
    is_certified: boolean
}