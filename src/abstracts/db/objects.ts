import type { Characteristic } from './characteristic';
import type { Domain } from './domain';

export interface ObjectsState {
    count: number,
    limit: number,
    next: string | null,
    previous: string | null,
    objects: Object[],
    ordering: string,
    search: string,
    offset: number
}

export interface Object {
    id: number,
    domain: Domain,
    characteristics: Characteristic[],
    name: string,
    description: string,
    criticality_level: number,
    archetypes: string
}
