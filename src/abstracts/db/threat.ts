import type {Attack} from "./attacks";
import type {Domain} from "./domain";
import type {Risk} from "./risks";
import type {Sfc} from "./sfc";
import type {Vulnerability} from "./vulnerability";

export interface Threat {
    id: number,
    attack: Attack,
    domains: Domain[],
    risks: Risk[],
    sfc: Sfc,
    vulnerability: Vulnerability,
    name: string,
    description: string,
    archetypes: string,
    object: number
}

export interface ThreatState {
    count: number,
    limit: number,
    next: string | null,
    previous: string | null,
    threats: Threat[],
    ordering: string,
    search: string,
    offset: number
}