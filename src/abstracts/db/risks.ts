import type { DamageType } from './damage-type';

export interface Risk {
    id: number,
    damage_type: DamageType[],
    name: string,
    description: string,
    criticality_level: number,
    archetypes: string
}

export interface RisksState {
    count: number,
    limit: number,
    next: string | null,
    previous: string | null,
    risks: Risk[],
    ordering: string,
    search: string,
    offset: number
}