export interface Weakness {
    id: number,
    name: string,
    vulnerabilities: number[]
}