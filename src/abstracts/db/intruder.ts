export interface Intruder {
    id: number,
    name: string,
    type: number,
    risk_types: [ number ]
}