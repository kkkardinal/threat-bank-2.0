import type {Tool} from "./tool";
import type {Consequence} from "./consequence";
import type {Intruder} from "./intruder";
import type {Motive} from "./motive";
import type {Image} from "./image";
import type {Possibility} from "./possibility";
import type {Purpose} from "./purpose";

export interface AttacksState {
    count: number,
    limit: number,
    next: string | null,
    previous: string | null,
    attacks: Attack[],
    ordering: string,
    search: string,
    offset: number
}

export interface Attack {
    id: number,
    tools: Tool[],
    consequences: Consequence[],
    intruders: Intruder[],
    motives: Motive[],
    images: Image[],
    possibilities: Possibility[],
    purposes: Purpose[],
    name: string,
    description: string,
    stage: number,
    capec_id: string,
    capec_url: string,
    impact_nature: number,
    impact_level: number,
    archetypes: string,
    weaknesses: [number]
}