export interface Domain {
    id: number,
    name: string,
    criticality_level: number
}