import { AuthData } from '../abstracts/auth-data';

export interface AuthState {
    authData: AuthData,
    user: {
        email: string,
        firstName: string,
        lastName: string,
        pk: number
    }
    loginStatus: string
}