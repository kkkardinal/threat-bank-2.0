export interface LinkBlock {
    image: string;
    name: string;
    link: string
}