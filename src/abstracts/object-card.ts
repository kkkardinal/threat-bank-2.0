export interface ObjectCard {
    title: string,
    url: string,
    label: string
}