export interface InfoBlock {
    image: string,
    name: string,
    description: string,
    path?: string
}