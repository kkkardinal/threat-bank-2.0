export interface ILimitItem {
    count: number,
    isActive: boolean
}