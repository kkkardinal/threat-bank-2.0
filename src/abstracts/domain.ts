export interface DomainsState {
    domains: Domain[],
    count: number
}

export interface Domain {
    id: number,
    name: string,
    criticality_level: number
}