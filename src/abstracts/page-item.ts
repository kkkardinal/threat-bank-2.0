export interface PageItem {
    route: string,
    img: string,
    title: string,
    description: string,
    isInvert: boolean,
    externalLink?s: string
}