import { computed, reactive } from 'vue';
import axios from 'redaxios';
import { jwtDecrypt, tokenAlive } from '../utils/jwtHelper';
import type { AuthData } from '../abstracts/auth-data';
import type { AuthState } from '../abstracts/auth-state';
import type { LoginData } from '../abstracts/login-data';
import { ApiRequest } from '../models/ApiRequest';
import type { RegisterFormData } from '../abstracts/form-data';
import {getHost} from "@/utils/get-host";

const apiRequest = new ApiRequest();

const authState = reactive<AuthState>({
    authData: {
        token: '',
        tokenExp: 0,
    },
    user: {
        email: '',
        firstName: '',
        lastName: '',
        pk: 0
    },
    loginStatus: ''
});

export function useAuthStore() {
    const setAuthData = (authData: AuthData) => {
        authState.authData = authData;
    }

    const setLoginStatus = (loginStatus: string) => {
        authState.loginStatus = loginStatus
    }

    const setUser = (user: any) => {
        authState.user = user
    }

    const authData = computed(() => authState.authData);
    const user = computed(() => authState.user)
    const loginStatus = computed(() => authState.loginStatus);

    const isTokenAlive = () => {
        return !authState.authData.tokenExp ? false : tokenAlive(authState.authData.tokenExp);
    }

    const login = async (data: LoginData) => {
        const response = await apiRequest.post<LoginData>('auth/login/', {
            email: data.email,
            password: data.password
        })

        if (response && response.data) {
            const user = response.data.user;
            setLoginStatus('success');
            setUser({
                email: user.email || '',
                firstName: user.first_name || '',
                lastName: user.last_name || '',
                pk: user.pk || ''
            })

            window.location.href = '/account'
            return;
        }

        setLoginStatus('failed');
    }

    const saveTokenData = (data: any) => {
        localStorage.setItem('access_token', data.accessToken);

        const jwtDecryption = jwtDecrypt(data.accessToken);

        authState.authData = {
            token: data.accessToken,
            tokenExp: jwtDecryption?.exp
        };
    }

    // const register = async (data: RegisterFormData) => {
    //     return apiRequest.post('auth/registration/', {
    //         email: data.email,
    //         password1: data.password,
    //         password2: data.repeatedPassword,
    //         agree_to_processing_of_personal_data: data.confirmation
    //     });
    // }

    const register = async (data: RegisterFormData) => {

            const response = await axios.post(
                getHost() + 'auth/registration/',
                {
                    email: data.email,
                    password1: data.password,
                    password2: data.repeatedPassword,
                    agree_to_processing_of_personal_data: data.confirmation,
                },
                {
                    withCredentials: true, // Для включения кук
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }
            );

            return response.data; // Возвращаем данные ответа
    };

    const refresh = async () => {
        return apiRequest.post('refresh/', {});
    }

    return {
        saveTokenData,
        login,
        register,
        isTokenAlive,
        loginStatus,
        authData,
        user,
        refresh,
        setLoginStatus,
        setAuthData,
        setUser
    }
}