import { computed, reactive } from 'vue';
import { ApiRequest } from '../models/ApiRequest';
import type {DomainsState, Domain} from "../abstracts/domain.ts";

const api = new ApiRequest();

const domainsState = reactive<DomainsState>({
    count: 0,
    domains: [],
});

export function useDomainsStore() {
    const setDomains = (domains: Domain[]) => {
        domainsState.domains = domains;
    }

    const setCount = (count: number) => {
        domainsState.count = count
    }

    const domains = computed(() => domainsState.domains);

    const getDomains = async () => {
        const domains = await api.get(`objects/domains/`);

        if (!domains) {
            return
        }

        setCount(domains.data.count);
        setDomains(domains.data.results);
    }


    return {
        setDomains,
        domains,
        getDomains,
    }
}