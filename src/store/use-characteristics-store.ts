import { computed, reactive } from 'vue';
import { ApiRequest } from '../models/ApiRequest';
import type { Characteristic, CharacteristicsState } from "../abstracts/characteristics.ts";

const api = new ApiRequest();

const characteristicsState = reactive<CharacteristicsState>({
    count: 0,
    characteristics: [],
});

export function useCharacteristicsStore() {
    const setCharacteristics = (characteristics: Characteristic[]) => {
        characteristicsState.characteristics = characteristics;
    }

    const setCount = (count: number) => {
        characteristicsState.count = count
    }

    const characteristics = computed(() => characteristicsState.characteristics);

    const getCharacteristics = async () => {
        const characteristics = await api.get(`objects/characteristics/`);

        if (!characteristics) {
            return
        }

        setCount(characteristics.data.count);
        setCharacteristics(characteristics.data.results);
    }


    return {
        setCharacteristics,
        characteristics,
        getCharacteristics,
    }
}