import { computed, reactive } from 'vue';
import { ApiRequest } from '../../models/ApiRequest';
import type { Sfc, UserDataState } from "../../abstracts/db/sfc";
import type { Vulnerability } from "../../abstracts/db/vulnerability.ts";
import type { Attack } from "../../abstracts/db/attacks.ts";
import axios from "redaxios";
import FileSaver from "file-saver";
import { getHost } from "@/utils/get-host";

const api = new ApiRequest();
const aapi = axios.create({
    url: getHost(),
    withCredentials: true
});
const userDataState = reactive<UserDataState>({
    sfcs: [],
    attacks: [],
    vulnerabilities: [],
    attackVectors: []
});

export function useUserDataStore() {
    const setUserSfcs = (sfcs: Sfc[]) => {
        userDataState.sfcs = sfcs;
    }

    const setUserVulnerabilities = (vulnerabilities: Vulnerability[]) => {
        userDataState.vulnerabilities = vulnerabilities;
    }

    const setUserAttacks = (attacks: Attack[]) => {
        userDataState.attacks = attacks;
    }

    const setUserAttackVectors = (attackVectors: any[]) => {
        userDataState.attackVectors = attackVectors;
    }

    const userSfcs = computed(() => userDataState.sfcs);
    const userVulnerabilities = computed(() => userDataState.vulnerabilities);
    const userAttacks = computed(() => userDataState.attacks);
    const userAttackVectors = computed(() => userDataState.attackVectors);

    const getUserAttackVectors = async (userId: number) => {
        try {
            const response = await api.get(`members/file_versions/?title_of_service=2&user=${userId}`);
            if (response && response.data && response.data.results) {
                setUserAttackVectors(response.data.results);
            }
        } catch (error) {
            console.error("Ошибка при загрузке векторов атак:", error);
        }
    };

    const getUserSfcs = async () => {
        const userSfcData = await api.get('members/file_versions/user_file_versions/');
        const sfcIds: any[] = [];

        if (!userSfcData) {
            return;
        }

        userSfcData.data.map((sfcData: any) => sfcData.sfcs.map((item: any) => sfcIds.push(item)));

        const userSfcs = await api.get(`sfc/characteristics?id__in=${sfcIds.toString()}`);

        if (!userSfcs) {
            return;
        }

        setUserSfcs(userSfcs.data.results);
    }

    const getUserVulnerabilities = async () => {
        const userVulnerabilities = await api.get('sfc/characteristics/sfc-with-vulnerabilities/my/');

        if (!userVulnerabilities) {
            return;
        }

        setUserVulnerabilities(userVulnerabilities.data.results);
    }

    const getUserAttacksFile = async () => {
        aapi.get(getHost() + 'sfc/characteristics/sfc-with-attacks/my/file-xlsx/', {
            responseType: "blob"
        }).then(response => {
            FileSaver.saveAs(response.data, 'attacks');
        });
    }

    const getUserVulnerabilitiesFile = () => {
        aapi.get(getHost() + 'sfc/characteristics/sfc-with-vulnerabilities/my/file-xlsx/', {
            responseType: "blob"
        }).then(response => {
            FileSaver.saveAs(response.data, 'vulnerabilities');
        });
    }

    const getCustomVulnerabilitiesFile = async (body: string) => {
        const response = await aapi.post(getHost() + 'sfc/characteristics/sfc-with-vulnerabilities/file-xlsx/', {
            sfcs_id: body
        }, { responseType: "blob" });

        FileSaver.saveAs(response.data, 'vulnerabilities');
    }

    const getCustomAttacksFile = async (body: string) => {
        const response = await aapi.post(getHost() + 'sfc/characteristics/sfc-with-attacks/file-xlsx/', {
            sfcs_id: body
        }, { responseType: "blob" });

        FileSaver.saveAs(response.data, 'attacks');
    }

    const getUserAttacks = async () => {
        const userAttacks = await api.get('sfc/characteristics/sfc-with-attacks/my/');

        if (!userAttacks) {
            return;
        }
        setUserAttacks(userAttacks.data.results);
    }

    const getUserSfcsById = (ids: string[]) => {
        return userSfcs.value.map((sfc: any) => {
            if (ids.includes(sfc.id.toString())) {
                return sfc;
            }
        });
    }

    const getAttackBySfcId = async (id: string) => {
        return api.post('sfc/characteristics/sfc-with-attacks/', {
            sfcs_id: id
        });
    };

    return {
        setUserSfcs,
        userSfcs,
        getUserSfcs,
        getUserSfcsById,
        userVulnerabilities,
        userAttacks,
        userAttackVectors,
        getUserVulnerabilities,
        getUserAttacks,
        setUserAttacks,
        getUserVulnerabilitiesFile,
        getCustomVulnerabilitiesFile,
        getUserAttacksFile,
        getCustomAttacksFile,
        getAttackBySfcId,
        getUserAttackVectors
    }
}
