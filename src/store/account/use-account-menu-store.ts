import { computed, reactive } from 'vue';

const accountMenuState = reactive({
    activeSection: 1
});

export function useAccountMenuStore() {
    const setActiveSection = (activeSection: number) => {
        accountMenuState.activeSection = activeSection;
    };

    const activeSection = computed(() => accountMenuState.activeSection);

    const changeActiveSection = (activeSection: number) => {
        setActiveSection(activeSection);
    };

    return {
        setActiveSection,
        activeSection,
        changeActiveSection
    }
}
