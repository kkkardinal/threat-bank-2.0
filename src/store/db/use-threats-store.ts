import { computed, reactive } from 'vue';
import { ApiRequest } from '../../models/ApiRequest';
import type {Threat, ThreatState} from "../../abstracts/db/threat";

const api = new ApiRequest();

const threatsState = reactive<ThreatState>({
    count: 0,
    limit: 10,
    next: '',
    previous: '',
    threats: [],
    ordering: '-id',
    search: '',
    offset: 0
});

export function useThreatsStore() {
    const setThreats = (threats: Threat[]) => {
        threatsState.threats = threats;
    }

    const setLimit = (limit: number) => {
        threatsState.limit = limit
    }

    const setOrdering = (ordering: string) => {
        threatsState.ordering = ordering
    }

    const setSearch = (search: string) => {
        threatsState.search = search
    }

    const setCount = (count: number) => {
        threatsState.count = count
    }

    const setOffset = (offset: number) => {
        threatsState.offset = offset
    }

    const threats = computed(() => threatsState.threats);
    const limit = computed(() => threatsState.limit);
    const ordering = computed(() => threatsState.ordering);
    const search = computed(() => threatsState.search);
    const count = computed(() => threatsState.count);
    const offset = computed(() => threatsState.offset);

    const getThreats = async () => {
        const threats = await api.get(`threats/threats/?ordering=${ordering.value}&limit=${limit}&search=${search.value}&offset=${offset.value}`);

        if (!threats) {
            return;
        }

        setCount(threats.data.count);
        setThreats(threats.data.results);
    }

    const pagesCount = computed(() => Math.ceil(count.value / limit.value) - 1);

    const sendThreatServiceData = async (data: any) => {
        return api.post('threats/threats/threats-service-xlsx/', data);
    };

    return {
        setThreats,
        setLimit,
        threats,
        getThreats,
        setOrdering,
        ordering,
        setSearch,
        search,
        offset,
        limit,
        pagesCount,
        setOffset,
        sendThreatServiceData
    }
}