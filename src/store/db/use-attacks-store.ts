import { computed, reactive } from 'vue';
import { ApiRequest } from '../../models/ApiRequest';
import type { Attack, AttacksState } from '../../abstracts/db/attacks';

const api = new ApiRequest();

const attacksState = reactive<AttacksState>({
    count: 0,
    limit: 10,
    next: '',
    previous: '',
    attacks: [],
    ordering: '-id',
    search: '',
    offset: 0
});

export function useAttackStore() {
    const setAttacks = (attacks: Attack[]) => {
        attacksState.attacks = attacks;
    }

    const setLimit = (limit: number) => {
        attacksState.limit = limit
    }

    const setOrdering = (ordering: string) => {
        attacksState.ordering = ordering
    }

    const setSearch = (search: string) => {
        attacksState.search = search
    }

    const setCount = (count: number) => {
        attacksState.count = count
    }

    const setOffset = (offset: number) => {
        attacksState.offset = offset
    }

    const attacks = computed(() => attacksState.attacks);
    const limit = computed(() => attacksState.limit);
    const ordering = computed(() => attacksState.ordering);
    const search = computed(() => attacksState.search);
    const count = computed(() => attacksState.count);
    const offset = computed(() => attacksState.offset);

    const getAttacks = async () => {
        const attacks = await api.get(`attacks/attacks/?ordering=${ordering.value}&limit=${limit.value}&search=${search.value}&offset=${offset.value}`);

        if (!attacks) {
            return
        }

        setCount(attacks.data.count);
        setAttacks(attacks.data.results);
    }

    const sendVectorAttack = async (result: any) => {
        return api.post('sfc/attackVectors/', result);
    }

    const pagesCount = computed(() => Math.ceil(count.value / limit.value) - 1);

    return {
        setAttacks,
        setLimit,
        attacks,
        ordering,
        search,
        pagesCount,
        offset,
        limit,
        getAttacks,
        setOrdering,
        setSearch,
        setOffset,
        sendVectorAttack
    }
}