import { computed, reactive } from 'vue';
import { ApiRequest } from '../../models/ApiRequest';
import type { Object, ObjectsState } from '../../abstracts/db/objects';

const api = new ApiRequest();

const objectsState = reactive<ObjectsState>({
    count: 0,
    limit: 10,
    next: '',
    previous: '',
    objects: [],
    ordering: '-id',
    search: '',
    offset: 0
});

export function useObjectsStore() {
    const setObjects = (objects: Object[]) => {
        objectsState.objects = objects;
    }

    const setLimit = (limit: number) => {
        objectsState.limit = limit
    }

    const setOrdering = (ordering: string) => {
        objectsState.ordering = ordering
    }

    const setSearch = (search: string) => {
        objectsState.search = search
    }

    const setCount = (count: number) => {
        objectsState.count = count
    }

    const setOffset = (offset: number) => {
        objectsState.offset = offset
    }

    const objects = computed(() => objectsState.objects);
    const limit = computed(() => objectsState.limit);
    const ordering = computed(() => objectsState.ordering);
    const search = computed(() => objectsState.search);
    const count = computed(() => objectsState.count);
    const offset = computed(() => objectsState.offset);

    const getObjects = async () => {
        const objects = await api.get(`objects/objects/?ordering=${ordering.value}&limit=${limit}&search=${search.value}&offset=${offset.value}`);

        if (!objects) {
            return
        }

        setCount(objects.data.count);
        setObjects(objects.data.results);
    }

    const pagesCount = computed(() => Math.ceil(count.value / limit.value) - 1);

    return {
        setObjects,
        setLimit,
        objects,
        offset,
        limit,
        getObjects,
        setOrdering,
        ordering,
        setSearch,
        search,
        pagesCount,
        setOffset
    }
}