import { computed, reactive } from 'vue';
import { ApiRequest } from '../../models/ApiRequest';
import type {Sfc, SfcState} from "../../abstracts/db/sfc";

const api = new ApiRequest();

const sfcsState = reactive<SfcState>({
    count: 0,
    limit: 10,
    next: '',
    previous: '',
    sfcs: [],
    ordering: '-id',
    search: '',
    offset: 0,
});

export function useSfcsStore() {
    const setSfcs = (sfcs: Sfc[]) => {
        sfcsState.sfcs = sfcs;
    }

    const setSfcsLimit = (limit: number) => {
        sfcsState.limit = limit
    }

    const setOrdering = (ordering: string) => {
        sfcsState.ordering = ordering
    }

    const setSearch = (search: string) => {
        sfcsState.search = search
    }

    const setCount = (count: number) => {
        sfcsState.count = count
    }

    const setOffset = (offset: number) => {
        sfcsState.offset = offset
    }

    const sfcs = computed(() => sfcsState.sfcs);
    const limit = computed(() => sfcsState.limit);
    const ordering = computed(() => sfcsState.ordering);
    const search = computed(() => sfcsState.search);
    const sfcCount = computed(() => sfcsState.count);
    const offset = computed(() => sfcsState.offset);

    const getSfcs = async () => {
        const sfcs = await api.get(`sfc/characteristics/?ordering=${ordering.value}&limit=${limit}&search=${search.value}&offset=${offset.value}`);

        if (!sfcs) {
            return
        }

        setCount(sfcs.data.count);
        setSfcs(sfcs.data.results);
    }

    const getSfcsWithoutLimit = async () => {
        const sfcs = await api.get(`sfc/characteristics/?ordering=${ordering.value}&limit=3000&search=${search.value}`);

        if (!sfcs) {
            return
        }

        setCount(sfcs.data.count);
        setSfcs(sfcs.data.results);
    }

    const getSfcsById = (ids: string[]) => {
        const convertedIds = JSON.parse(JSON.stringify(ids)).map((item: number) => item.toString());

        return sfcs.value.map(sfc => {
            if (convertedIds.includes(sfc.id.toString())) {
                return JSON.parse(JSON.stringify(sfc));
            }

        }).filter(Boolean)
    }

    const pagesCount = computed(() => Math.ceil(sfcCount.value / limit.value) - 1);

    return {
        setSfcs,
        setSfcsLimit,
        sfcs,
        limit,
        getSfcs,
        setOrdering,
        ordering,
        setSearch,
        search,
        offset,
        pagesCount,
        setOffset,
        getSfcsById,
        sfcCount,
        getSfcsWithoutLimit
    }
}