import { computed, reactive } from 'vue';
import { ApiRequest } from '../../models/ApiRequest';
import type {Vulnerability, VulnerabilityState} from "../../abstracts/db/vulnerability";

const api = new ApiRequest();

const vulnerabilitiesState = reactive<VulnerabilityState>({
    count: 0,
    limit: 10,
    next: '',
    previous: '',
    vulnerabilities: [],
    ordering: '-id',
    search: '',
    offset: 0
});

export function useVulnerabilitiesStore() {
    const setVulnerabilities = (vulnerabilities: Vulnerability[]) => {
        vulnerabilitiesState.vulnerabilities = vulnerabilities;
    }

    const setLimit = (limit: number) => {
        vulnerabilitiesState.limit = limit
    }

    const setOrdering = (ordering: string) => {
        vulnerabilitiesState.ordering = ordering
    }

    const setSearch = (search: string) => {
        vulnerabilitiesState.search = search
    }

    const setCount = (count: number) => {
        vulnerabilitiesState.count = count
    }

    const setOffset = (offset: number) => {
        vulnerabilitiesState.offset = offset
    }

    const vulnerabilities = computed(() => vulnerabilitiesState.vulnerabilities);
    const limit = computed(() => vulnerabilitiesState.limit);
    const ordering = computed(() => vulnerabilitiesState.ordering);
    const search = computed(() => vulnerabilitiesState.search);
    const count = computed(() => vulnerabilitiesState.count);
    const offset = computed(() => vulnerabilitiesState.offset);

    const getVulnerabilities = async () => {
        const vulnerabilities = await api.get(`vulnerabilities/vulnerabilities/?ordering=${ordering.value}&limit=${limit}&search=${search.value}&offset=${offset.value}`);

        if (!vulnerabilities) {
            return
        }

        setCount(vulnerabilities.data.count);
        setVulnerabilities(vulnerabilities.data.results);
    }

    const pagesCount = computed(() => Math.ceil(count.value / limit.value) - 1);

    return {
        setVulnerabilities,
        setLimit,
        vulnerabilities,
        getVulnerabilities,
        setOrdering,
        ordering,
        setSearch,
        search,
        pagesCount,
        offset,
        limit,
        setOffset
    }
}