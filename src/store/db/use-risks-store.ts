import { computed, reactive } from 'vue';
import { ApiRequest } from '../../models/ApiRequest';
import type { Risk, RisksState } from '../../abstracts/db/risks';

const api = new ApiRequest();

const risksState = reactive<RisksState>({
    count: 0,
    limit: 10,
    next: '',
    previous: '',
    risks: [],
    ordering: '-id',
    search: '',
    offset: 0
});

export function useRisksStore() {
    const setRisks = (risks: Risk[]) => {
        risksState.risks = risks;
    }

    const setLimit = (limit: number) => {
        risksState.limit = limit
    }

    const setOrdering = (ordering: string) => {
        risksState.ordering = ordering
    }

    const setSearch = (search: string) => {
        risksState.search = search
    }

    const setCount = (count: number) => {
        risksState.count = count
    }

    const setOffset = (offset: number) => {
        risksState.offset = offset
    }

    const risks = computed(() => risksState.risks);
    const limit = computed(() => risksState.limit);
    const ordering = computed(() => risksState.ordering);
    const search = computed(() => risksState.search);
    const count = computed(() => risksState.count);
    const offset = computed(() => risksState.offset);

    const getRisks = async () => {
        const risks = await api.get(`risks/risks/?ordering=${ordering.value}&limit=${limit}&search=${search.value}&offset=${offset.value}`);

        if (!risks) {
            return
        }

        setCount(risks.data.count);
        setRisks(risks.data.results);
    }

    const pagesCount = computed(() => Math.ceil(count.value / limit.value) - 1);

    return {
        setRisks,
        setLimit,
        risks,
        getRisks,
        setOrdering,
        ordering,
        setSearch,
        search,
        offset,
        limit,
        pagesCount,
        setOffset
    }
}