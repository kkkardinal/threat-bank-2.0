import { getNames } from '../utils/get-names';

export const getMotiveNames = (intruders: any) => {
    const getMotives = () => {
        return intruders.map((intruder: any) => intruder.motives)
    }

    const [motives] = getMotives();

    return getNames(motives);
}