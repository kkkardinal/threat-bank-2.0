import { getUniqueNames } from '../utils/get-unique-names';

export const getTranslatedNames =  (data: any) => {
    const array = data || [];
    const names = array.map((item: any) => item.name_translated + '');

    return getUniqueNames(names);
}