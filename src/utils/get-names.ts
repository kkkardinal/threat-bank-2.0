import { getUniqueNames } from '../utils/get-unique-names';

export const getNames = (data: any) => {
    if (!data) {
        return;
    }

    const names = data.map((item: any) => item.name + '');

    return getUniqueNames(names);
}