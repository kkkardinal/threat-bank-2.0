import { jwtDecode } from "jwt-decode";
import type { JwtDecryption } from '../abstracts/jwt-decryption';

export const jwtDecrypt = (token: string) => {
    return jwtDecode(token) as JwtDecryption;
}

export function tokenAlive(exp: number): boolean {
    return Date.now() < exp * 1000;
}
