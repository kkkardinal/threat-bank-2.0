export const getUniqueNames = (names: any) => {
    return  names.filter((item: any, pos: any) => {
        return names.indexOf(item) == pos;
    }).join(', ');
}